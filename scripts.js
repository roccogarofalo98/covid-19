fetch('https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-json/dpc-covid19-ita-regioni.json')
.then(response => response.json())
.then(dati => {
    // ordino i dati per quello piu recente
    let sorted = dati.reverse()
    
    // prendo l'ultima data
    let lastUpdate = sorted[0].data
    
    // formattazione della data
    let lastUpdateFormatted = lastUpdate.split('T')[0].split('-').reverse().join('/')
    
    // inserisco l'ultima data nel p
    document.querySelector('#data').innerHTML = lastUpdateFormatted
    
    // filtrare tutto per ultima data e per i nuovi casi 
    lastUpdateData = sorted.filter(el => el.data == lastUpdate).sort((a, b) => b.nuovi_positivi - b.nuovi_positivi)
    
    // totali casi
    let totalCases = lastUpdateData.map(el => el.totale_casi).reduce((t, n) => t + n)
    document.querySelector('#casi_totali').innerHTML = totalCases
    
    // tatali guariti
    let totalRecovered = lastUpdateData.map(el => el.dimessi_guariti).reduce((t, n) => t + n)
    document.querySelector('#totalRecovered').innerHTML = totalRecovered
    
    // tatali decessi
    let totalDied = lastUpdateData.map(el => el.deceduti).reduce((t, n) => t + n)
    document.querySelector('#totalDied').innerHTML = totalDied
    
    // totali positivi
    let totalPositive = lastUpdateData.map(el => el.totale_positivi).reduce((t, n) => t + n)
    document.querySelector('#totalPositive').innerHTML = totalPositive
    
    // Per regioni
    let cardWrapper = document.querySelector('#cardWrapper')
    lastUpdateData.forEach(el =>{
        
        let div = document.createElement('div')
        div.classList.add('col-12', 'col-md-6', 'my-4')
        div.innerHTML=
        `
        <div class="card-custom p-3 pb-3 h-100" data-region="${el.denominazione_regione}">
        <p class="text-seco">${el.denominazione_regione}</p>
        <h4 class="text-right mb-0 text-main">${el.nuovi_positivi}</h4>
        </div>
        `
        cardWrapper.appendChild(div)
    })
    
    let progressWrapper = document.querySelector('#progressWrapper')
    let todayMax = Math.max(...lastUpdateData.map(el=>el.nuovi_positivi))
    
    
    lastUpdateData.forEach(el=>{
        
        let div = document.createElement('div')
        div.classList.add('col-12', 'mb-5')
        div.innerHTML=
        `
        <p class="mb-0 text-seco">${el.denominazione_regione}: ${el.nuovi_positivi}</p>
        <div class="progress">
        <div class="progress-bar bg-main" style="width: ${100 * el.nuovi_positivi/todayMax}%;">
        </div>
        </div>
        `
        progressWrapper.appendChild(div)
        
    })
    
    
    let modal = document.querySelector('.modal-custom')
    let modalContent = document.querySelector('.modal-custom-content')
    
    document.querySelectorAll('[data-region').forEach(el => {
        el.addEventListener('click', ()=>{
            
            let region = el.dataset.region
            modal.classList.add('active')
            
            
            let dataAboutRegion = lastUpdateData.filter(el => el.denominazione_regione == region)[0]
            
            modalContent.innerHTML = 
            `
            <div class="container">
            <div class="row">
            <div class="col-12">
            <p class="h2 text-main">${dataAboutRegion.denominazione_regione}</p>
            </div>
            <div class="col-12">
            <p class="lead">Totale Casi: ${dataAboutRegion.totale_casi}</p>
            <p class="lead">Nuovi Positivi: ${dataAboutRegion.nuovi_positivi}</p>
            <p class="lead">Deceduti: ${dataAboutRegion.deceduti}</p>
            <p class="lead">Guariti: ${dataAboutRegion.dimessi_guariti}</p>
            <p class="lead">Ricoverati con Sintomi: ${dataAboutRegion.ricoverati_con_sintomi}</p>
            </div>
            </div>
            </div>
            `
        })
    })
    
    window.addEventListener('click', function(e){
        if(e.target == modal){
            modal.classList.remove('active')      
        }
    })
    
    console.log(lastUpdateData)
})